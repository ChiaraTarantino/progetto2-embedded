#ifndef __COFFEE_MACHINE_TASK__
#define __COFFEE_MACHINE_TASK__

#include "Task.h"
#include "Machine.h"
#include "Customer.h"
#include "Led.h"

class CoffeeMachineTask: public Task{

public:
  CoffeeMachineTask(Customer* customer, Machine* machine);
  void init(int period);
  void tick();

private:
  Customer* customer;
  Machine* machine;
  Led* l1;
  Led* l2;
  Led* l3;

  int timeBeforePowerSave;
  int timeMakingCoffee;
  int timeCoffeeDone;

  void checkAndUpdateLight();

  enum { STANDBY, ON, READY, MAKING_COFFEE, COFFEE_DONE, MAINTENANCE} state;

};

#endif
