#ifndef __COFFEE_REFILLER_TASK__
#define __COFFEE_REFILLER_TASK__

#include "Task.h"
#include "Machine.h"
#include "Arduino.h"

class CoffeeRefillerTask: public Task {

public:
  CoffeeRefillerTask(Machine* machine);
  void init(int period);
  void tick();

private:
  Machine* machine;
  int parse(String msg);

};


#endif
