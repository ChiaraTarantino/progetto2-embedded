#ifndef __COMMUNICATION_SERVICE__
#define __COMMUNICATION_SERVICE__

#include "Arduino.h"

class CommunicationService {

public:
  void log(const String& msg);
  void clear();
  void sendSugarLevel(const int level);
  void sendIsMaintenance(const bool maintenance);
};

extern CommunicationService Communication;

#endif
