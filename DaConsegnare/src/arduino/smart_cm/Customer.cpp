#include "Customer.h"
#include "Arduino.h"

Customer::Customer() {
  present = false;
  close = false;
  takenCoffee = false;
}

bool Customer::isPresent() {
  return present;
}

bool Customer::isGoneAway() {
  return !(isPresent());
}

bool Customer::isClose() {
  return close;
}

bool Customer::isFar() {
  return !isClose();
}

bool Customer::hasTakenCoffee() {
  bool wasTakenCoffee = takenCoffee;
  takenCoffee = false;
  return wasTakenCoffee;
}

void Customer::setPresent() {
  present = true;
}

void Customer::setGoneAway() {
  present = false;
}

void Customer::setClose() {
  close = true;
}

void Customer::setFar() {
  close = false;
}

void Customer::takeCoffee() {
  takenCoffee = true;
}
