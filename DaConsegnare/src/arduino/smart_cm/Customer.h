#ifndef __CUSTOMER__
#define __CUSTOMER__

/*
 * Classe che rappresenta lo stato attuale del cliente della macchina.
 */
class Customer {
public:

  /*
   * Costruttore senza parametri che imposta lo stato iniziale,
   * ovvero clienti assenti.
   */
  Customer();

  /* 
   * Ritorna vero se ci sono clienti davanti alla macchina.
   */
  bool isPresent();

  /*
   * Ritorna vero quando non ci sono clienti davanti alla macchina.
   */
  bool isGoneAway();

  /*
   * Ritorna vero quando ci sono clienti vicini alla macchina.
   */
  bool isClose();

  /*
   * Ritorna vero quando non ci sono clienti vicini alla macchina.
   */
  bool isFar();

  /*
   * Ritorna vero quando il cliente ha preso il caffe', poi si resetta a falso.
   */
  bool hasTakenCoffee();

  /*
   * Setter per lo stato del cliente.
   */
  void setPresent();
  void setGoneAway();
  void setClose();
  void setFar();

  /*
   * Setter per l'evento preso il caffe'.
   */
  void takeCoffee();

private:
  bool present;
  bool close;
  bool takenCoffee;
};
#endif /* ifndef __CUSTOMER__ */
