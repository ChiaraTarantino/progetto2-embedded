#include "CustomerHandlerTask.h"
#include "CommunicationService.h"
#include "config.h"
#include "Arduino.h"
#include "Sonar.h"
#include "Pir.h"
#include "ButtonImpl.h"

CustomerHandlerTask::CustomerHandlerTask(Customer* customer,Machine* machine){
  this->customer = customer;
  this->machine = machine;
  this->sonar = new Sonar(ECHO_PIN,TRIG_PIN);
  this->button = new ButtonImpl(BUTTON_PIN);
  this->detector = new Pir(PIR_PIN);
  this->pot = new Pot(POT_PIN);
}

void CustomerHandlerTask::init(int period){
  Task::init(period);
  state = AWAY;
  this->lastDistance=0;
  this->arrivalTime=0;
  this->noPresenceTime=0;
  this->mapSugar=DEFAULT_SUGAR;   
  this->lastDistance=0;
}

void CustomerHandlerTask::tick(){
  switch(state){
    
     case AWAY: {
        bool clientDetected = detector->isDetected();
        if(machine->isInService() && clientDetected){
          customer->setPresent();
          state = PRESENT;
          arrivalTime = 0;
          noPresenceTime = 0;
        }
        break;
     }
     
     case PRESENT: {
        float distance = checkDistance();
        bool clientDetected = detector->isDetected();
        if(distance < DIST1){
          arrivalTime += myPeriod;
          noPresenceTime = 0;
          if(distance < DIST1 && arrivalTime > DT1){
            state = APPROACHING;
            customer->setClose();
            arrivalTime = 0;
          }
        }else if(!clientDetected){
          noPresenceTime += myPeriod;
          arrivalTime = 0;
          if(!clientDetected && noPresenceTime > DT2B){
             state = AWAY;
             noPresenceTime = 0;
             customer->setGoneAway();
          }
        }else{
          arrivalTime = 0;      /*distance > DIST1*/
          noPresenceTime = 0;
        }
        break;
     }

     case APPROACHING: {
        float distance = checkDistance();
        bool buttonPressed = button->isPressed();
        if(mapSugar != map(pot->getValue(), 0, 1023, 0, MAX_SUGAR)) {
          mapSugar=map(pot->getValue(), 0, 1023, 0, MAX_SUGAR);
          Communication.sendSugarLevel(mapSugar);
        }
        if(distance > DIST1){
          arrivalTime += myPeriod;
          if(arrivalTime > DT2A){
           arrivalTime = 0;
           state = PRESENT;
           customer->setFar();
          }
        }else if(distance < DIST1){
          arrivalTime = 0;
        }

        if(buttonPressed && machine->isInService()){
          state = WAITING;
          machine->setButtonPressed();
        }else if(!machine ->isInService()){
          customer->setFar();
          customer->setGoneAway();
          state = AWAY;
        }
        break;
     }
     
     case WAITING: {
        if(machine->isDone()){
          state = CAN_TAKE;
        }
        break;
     }
     
     case CAN_TAKE: {
       float distance = checkDistance();
       if(distance < DIST2){
        customer->takeCoffee();
        state = APPROACHING;
       }else if(machine->isTimoutElapsed()){
          state = APPROACHING;
       }
        break;
     }
  }
}

float CustomerHandlerTask::checkDistance(){
  float distance = sonar->getDistance();
  if (distance - lastDistance > DIST_MIN){
    distance = sonar->getDistance();
  }
  lastDistance = distance;
  return distance;
}
