#include "Pot.h"
#include "Arduino.h"

Pot::Pot(int pin){
  this->pin = pin;
  pinMode(pin, INPUT);     
} 
  
int Pot::getValue(){
  return analogRead(pin);
}
