#ifndef __POTENZIOMETRO__
#define __POTENZIOMETRO__

#include "PositionSensor.h"

class Pot: public PositionSensor {

public:  
  Pot(int pin);
  int getValue();
  
private:
    int pin;
};

#endif 
