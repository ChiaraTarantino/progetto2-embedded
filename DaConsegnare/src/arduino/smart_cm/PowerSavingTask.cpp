#include "PowerSavingTask.h"
#include "Pir.h"
#include "config.h" 
#include "Arduino.h"
#include <avr/sleep.h>
#include <avr/power.h>

PowerSavingTask::PowerSavingTask(Machine* machine){
  this->machine = machine;
}

void PowerSavingTask::init(int period){
  Task::init(period);
  state = WAITING;
}

void wakeUp(){}

void PowerSavingTask::tick(){
  switch(state){
     case WAITING:{ 
        if(machine->isInStandby()){
          state = STANDBY;
        }
        break;
     }
     
     case STANDBY: {
        state = WAITING; 
        attachInterrupt(digitalPinToInterrupt(PIR_PIN),wakeUp, RISING);
        this->setSleepMode();
        break;
     }        
  }
}

void PowerSavingTask::setSleepMode(){
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  sleep_mode();
  /*quando esce dallo sleep continua quì*/
  sleep_disable();
}
