#ifndef __CONFIG__
#define __CONFIG__

#define POT_PIN A2 //Analogico
#define PIR_PIN 3
#define BUTTON_PIN 6
#define ECHO_PIN 7
#define TRIG_PIN 8
#define L3_PIN 10
#define L2_PIN 11
#define L1_PIN 12

/*Utilizzati in CustomerHandlerTask*/
#define DIST_MIN 10
#define DIST1 0.3 //in m
#define DIST2 0.1 //in m
#define DT1 1000 //ms
#define DT2A 5000 //ms
#define DT2B 5000 //ms
#define MAX_SUGAR 5
#define DEFAULT_SUGAR 2.5

/*Utilizzati in CoffeeMachineTask*/
#define COFFEE_MAKING_DURATION 3000  // DT3 in ms
#define MAX_TIME_REMOVE_COFFEE 5000 // DT4 in ms
#define SYNCHRONIZATION_TIME 1500

#endif
