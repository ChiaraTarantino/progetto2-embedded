package smartCoffeeMachine;
import javafx.stage.Stage;
import jssc.SerialPortList;
import javafx.scene.Scene;
import javafx.scene.Parent;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import java.lang.Thread;

public class MainApplication extends Application {
	
  private static String LOG_MESSAGE = "LOG";
  private static String SUGAR_MESSAGE = "SUGAR";
  private static String MAINTENANCE_MESSAGE = "MAINTENANCE";
  private static String CLEAR_MESSAGE = "CLEAR";
  private static float MAX_SUGAR = 5; 
  private static float DEFAULT_SUGAR = 2/MAX_SUGAR;
  private SerialCommChannel com;
  private Controller controller;
  
  @Override
  public void start(Stage primaryStage) throws Exception {
	/*load dell'fxml*/
    FXMLLoader loader = new FXMLLoader(getClass().getResource("/GUI.fxml"));
    Parent root = loader.load();
    controller = ((GuiController)loader.getController());
    
    controller.setProgressBar(DEFAULT_SUGAR);	//setto il valore iniziale dello zucchero
    detectPorts(); // inizializza la Combobox con la lista delle porte
    
    /*creo un nuovo commChannel con relativo consumer */
    com = new SerialCommChannel(controller.getSelectValue(), 9600);
    controller.registerConsumer(com::sendMsg);

    //aggiungo il listener al variare del valore della ComboBox
    controller.onSelectChangeValue(new ChangeListener<String>() {
    	@Override
    	public void changed(ObservableValue<? extends String> observable, 
        String oldValue, String newValue) {
    		com.close();
    		try {
          com = new SerialCommChannel(newValue, 9600);
          controller.registerConsumer(com::sendMsg);
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    });
    
    new Thread(new Runnable() {
      @Override
      public void run() {
        while(true) {
          try {
            if (com.isMsgAvailable()) {
              String msg = com.receiveMsg();
              parse(msg);          
            }
          } catch(Exception e){
            e.printStackTrace();
          }
        }
      }
      
      private void parse(String msg){
        String[] cmd = msg.split(" ", 2);
        if (cmd.length == 2) {
          if (cmd[0].equals(LOG_MESSAGE)) {
            controller.println(cmd[1].trim());
          } else if (cmd[0].equals(SUGAR_MESSAGE)) {
            int arg = Integer.parseInt(cmd[1].trim());
            controller.setProgressBar((float) arg / MAX_SUGAR);
          } else if (cmd[0].equals(MAINTENANCE_MESSAGE)) {
            controller.isInMaintenance(cmd[1].trim().equals("1") ? true : false);
          } 
        } else if (cmd[0].trim().equals(CLEAR_MESSAGE)) { // questo non ha argomenti
          controller.clear();
        }
      }
    }).start();
    
    primaryStage.setOnCloseRequest((e) -> System.exit(0));
    primaryStage.setResizable(false);
    primaryStage.setHeight(800);
    primaryStage.setWidth(784);
    primaryStage.setTitle("Smart Coffee Machine");
    primaryStage.setScene(new Scene(root));
    primaryStage.show();
}
  
  private void detectPorts() {
	  String[] ports = SerialPortList.getPortNames();
	  controller.fillSelect(ports);      
  }

  public static void main(String args[]) {
    launch(args);
  }
  
}
