#include "CoffeeMachineTask.h"
#include "CommunicationService.h"
#include "config.h"
#include "Arduino.h"

CoffeeMachineTask::CoffeeMachineTask(Customer* customer, Machine* machine){
  this->customer = customer;
  this->machine = machine;
  this->l1= new Led(L1_PIN);
  this->l2= new Led(L2_PIN);
  this->l3= new Led(L3_PIN);
}

void CoffeeMachineTask::init(int period){
  Task::init(period);
  state = STANDBY;
  this->timeBeforePowerSave=0;
  this->timeMakingCoffee=0;
  this->timeCoffeeDone=0;
}

void CoffeeMachineTask::tick(){
  switch(state){
    case STANDBY:{
      timeBeforePowerSave += myPeriod;
      if(customer->isPresent()){
          state = ON;
          timeBeforePowerSave = 0;
      } else if (timeBeforePowerSave > SYNCHRONIZATION_TIME) {
           timeBeforePowerSave = 0;
           machine->setStandby();
      }
      break;
    }

    case ON:{
      if(customer->isClose()){
          Communication.log("Welcome");
          state=READY;
        }
        else if(customer->isGoneAway()){
          state=STANDBY;
        }
      break;
    }

    case READY:{
      if(machine->isCoffeeFinished()){
          Communication.log("No more coffee. Waiting for recharge");
          Communication.sendIsMaintenance(true);
          state=MAINTENANCE;
          machine->setMaintenance();
      } else if(machine->isButtonPressed()){
          state=MAKING_COFFEE;
          Communication.log("Making a coffee");
          machine->setWorking();
      } else if (customer->isFar()){
          Communication.clear();
          state=ON;
      }
      break;
    }

    case MAKING_COFFEE:{
      timeMakingCoffee += myPeriod;
      checkAndUpdateLight();
      if(timeMakingCoffee >= COFFEE_MAKING_DURATION){
        timeMakingCoffee = 0;
        Communication.log("The Coffee is ready");
        machine->setDone();
        state=COFFEE_DONE;
      }
      break;
    }

    case COFFEE_DONE:{
      timeCoffeeDone += myPeriod;
      if(timeCoffeeDone >= MAX_TIME_REMOVE_COFFEE){
        timeCoffeeDone = 0;
        Communication.clear();
        state=READY;
        Communication.log("Welcome");
        machine->setTimoutElapsed();
      } else if(customer->hasTakenCoffee()){
        Communication.clear();
        state=READY;
        Communication.log("Welcome");
      }
       break;
    }

    case MAINTENANCE:{
      if(!(machine->isCoffeeFinished())){
        String nc = "Coffee Refilled: +";
        nc+= machine->getCapsulesNumber();
        Communication.log(nc);
        Communication.sendIsMaintenance(false);
        state=STANDBY;
        machine->setInService();
      }
      break;
    }
  }
}

void CoffeeMachineTask::checkAndUpdateLight(){
  if (timeMakingCoffee < COFFEE_MAKING_DURATION / 3) {
    l1->switchOn();
  } else if (timeMakingCoffee < COFFEE_MAKING_DURATION * 2 / 3) {
    l1->switchOff();
    l2->switchOn();
  } else if (timeMakingCoffee < COFFEE_MAKING_DURATION) {
    l2->switchOff();
    l3->switchOn();
  } else {
    l3->switchOff();
  }
}
