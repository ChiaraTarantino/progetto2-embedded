#include "CoffeeRefillerTask.h"
#include "MsgService.h"
#include "Arduino.h"

CoffeeRefillerTask::CoffeeRefillerTask(Machine* machine){
  this->machine = machine;
}

void CoffeeRefillerTask::init(int period){
  Task::init(period);
}

void CoffeeRefillerTask::tick(){
  if (machine->isInMaintenance() && MsgService.isMsgAvailable()) {
    Msg* msg = MsgService.receiveMsg();
    int num = parse(msg->getContent());
    if (num) {
      machine->refill(num);
    }
  }
}

int CoffeeRefillerTask::parse(String m) {
  return m.toInt();
}
