#include "CommunicationService.h"
#include "MsgService.h"
#include "Arduino.h"

void CommunicationService::log(const String& msg) {
  MsgService.sendMsg("LOG " + msg);
}

void CommunicationService::clear() {
  MsgService.sendMsg("CLEAR");
}

void CommunicationService::sendSugarLevel(const int level) {
  MsgService.sendMsg("SUGAR " + String(level));
}

void CommunicationService::sendIsMaintenance(const bool maintenance) {
  String msg = "MAINTENANCE ";
  msg += maintenance ? "1" : "0";
  MsgService.sendMsg(msg);
}
