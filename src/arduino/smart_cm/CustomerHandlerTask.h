#ifndef __CUSTOMER_HANDLER_TASK__
#define __CUSTOMER_HANDLER_TASK__

#include "Task.h"
#include "Customer.h"
#include "Machine.h"
#include "ProximitySensor.h"
#include "PresenceSensor.h"
#include "Button.h"
#include "Pot.h"


class CustomerHandlerTask: public Task{

  public:
    CustomerHandlerTask(Customer* customer,Machine* machine);
    void init(int period);
    void tick();

  private:
   
    Customer* customer;
    Machine* machine;
    PresenceSensor* detector;
    ProximitySensor* sonar;
    Button* button;
    Pot* pot;
    
    int arrivalTime;
    int noPresenceTime;
    float mapSugar;
    float lastDistance;
    
    float checkDistance();
    
    enum { AWAY, PRESENT, APPROACHING, WAITING, CAN_TAKE} state;
};

#endif
