#include "Machine.h"
#include "Arduino.h"

Machine::Machine() {
  inService = true;
  buttonPressed = false;
  working = false;
  done = false;
  timoutElapsed = false;
  capsulesNumber = NMAX;
  energySaving = false;
}

bool Machine::isInService() {
  return inService;
}

bool Machine::isInMaintenance() {
  return !isInService();
}

bool Machine::isButtonPressed() {
  bool wasButtonPressed = buttonPressed;
  buttonPressed = false;
  return wasButtonPressed;
}

bool Machine::isDone() {
  bool wasDone = done;
  done = false;
  return wasDone;
}

bool Machine::isTimoutElapsed() {
  bool wasTimoutElapsed = timoutElapsed;
  timoutElapsed = false;
  return wasTimoutElapsed;
}

bool Machine::isCoffeeFinished(){
  return capsulesNumber == 0;
}

int Machine::getCapsulesNumber(){
  return capsulesNumber;  
}

void Machine::decrementNumberCapsule(){
  capsulesNumber-=1;
}

void Machine::setInService() {
  inService = true;
}

void Machine::setMaintenance() {
  inService = false;
}

void Machine::refill(int n) {
  this->capsulesNumber += n;
}

void Machine::setButtonPressed() {
  this->buttonPressed = true;
}

void Machine::setWorking() {
  working = true;
}

void Machine::setDone() {
  done = true;
  decrementNumberCapsule();
}

void Machine::setTimoutElapsed() {
  timoutElapsed = true;
}

void Machine::setStandby(){
  energySaving = true;
}

bool Machine::isInStandby(){
  bool wasEnergySaving = energySaving;
  energySaving = false;
  return wasEnergySaving;
}
