#ifndef __MACHINE__
#define __MACHINE__

#define DEFAULT_SUGAR 2
#define NMAX 10

/*
 * Classe che rappresenta lo stato attuale della macchina.
 */
class Machine {
public:

  /*
   * Costruttore senza parametri che imposta lo stato iniziale:
   * macchina in servizio in standby.
   */
  Machine();

  /*
   * Ritorna vero se la macchina e' in servizio.
   */
  bool isInService();

  /*
   * Ritorna vero se la macchina e' in manutenzione.
   */
  bool isInMaintenance();

  /*
   * Ritorna vero quando il bottone e' stato premuto, poi si resetta a falso.
   */
  bool isButtonPressed();
  /*
   * Ritorna vero quando la macchina ha finito di preparare un caffe', poi si
   * resetta a falso.
   */
  bool isDone();

  /*
   * Ritorna vero se il tempo a disposizione per prendere il caffe' e' scaduto,
   * poi si resetta a falso.
   */
  bool isTimoutElapsed();

  /*
   * Se il numero delle capsule è a 0 ritorna vero, altrimenti falso.
   */
  bool isCoffeeFinished();
  
  /*
   * Ritorna il numero di capsule 
   */
  int getCapsulesNumber();
  
   /*
   * Setter per lo stato.
   */
  void setInService();
  void setMaintenance();
  void refill(int n);

  /*
   *
   *Setter StandBy
   */

  void setStandby();
  bool isInStandby();
  /*
   * Setter per gli eventi.
   */
  void setButtonPressed();
  void setWorking();
  void setDone();
  void setTimoutElapsed();
  
private:
  bool inService;
  bool buttonPressed;
  bool working;
  bool done;
  bool timoutElapsed;
  int capsulesNumber;
  bool energySaving;
  /*
   * Decrementa di uno il numero di capsule di caffè della macchinetta.
   */
  void decrementNumberCapsule();
};

#endif 
