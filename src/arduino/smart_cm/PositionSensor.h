#ifndef __POSITIONSENSOR__
#define __POSITIONSENSOR__

class PositionSensor {
 
public: 
  virtual int getValue() = 0;
};

#endif
