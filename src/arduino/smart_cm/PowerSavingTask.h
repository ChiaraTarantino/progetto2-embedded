#ifndef __POWER_SAVING_TASK__
#define __POWER_SAVING_TASK__

#include "Task.h"
#include "Machine.h"

class PowerSavingTask: public Task{

  public:
    PowerSavingTask(Machine* machine);
    void init(int period);
    void tick();

  private:
    Machine* machine;
    void setSleepMode();
    
    enum { WAITING , STANDBY} state;
};

#endif
