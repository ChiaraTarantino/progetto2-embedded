/*
 * authors: Filippo Nardini, Luca Sambuchi, Chiara Tarantino
 *
*/

#include "Scheduler.h"
#include "Machine.h"
#include "Customer.h"
#include "CustomerHandlerTask.h"
#include "CoffeeMachineTask.h"
#include "CoffeeRefillerTask.h"
#include "PowerSavingTask.h"
#include "CommunicationService.h"
#include "MsgService.h"

Scheduler sched;

void setup() {
  sched.init(50);

  MsgService.init();
  Machine* machine=new Machine();
  Customer* customer= new Customer();

  CustomerHandlerTask* custHandlerTask = new CustomerHandlerTask(customer,machine);
  custHandlerTask->init(50);
  sched.addTask(custHandlerTask);
  
  CoffeeMachineTask* coffeeMachTask = new CoffeeMachineTask(customer,machine);
  coffeeMachTask->init(50);
  sched.addTask(coffeeMachTask);
  
  PowerSavingTask* powerSavingTask = new PowerSavingTask(machine);
  powerSavingTask->init(150);
  sched.addTask(powerSavingTask);

  CoffeeRefillerTask* coffeeRefillerTask = new CoffeeRefillerTask(machine);
  coffeeRefillerTask->init(150);
  sched.addTask(coffeeRefillerTask);
}

void loop() {
  sched.schedule();
}
