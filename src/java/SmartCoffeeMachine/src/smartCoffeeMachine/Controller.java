package smartCoffeeMachine;
import java.util.function.Consumer;
import javafx.beans.value.ChangeListener;

public interface Controller {
  
  /* registra il consumatore della stringa inserita nella textBox */
  void registerConsumer(Consumer<String> consumer);
  
  /*setta il valore della progress bar */
  void setProgressBar(float percentage);
  
  /*stampa nella textArea*/
  void println(String msg);

  /* pulisce la textArea */
  void clear();
  
  /*controlla se sono state inserite capsule oppure ancora no e gestisce
   * l'attivazione e disattivazione del bottone */
  void isInMaintenance(boolean maintenance);
  
  /* inizializza la ComboBox con la lista delle porte seriali*/
  void fillSelect(String[] ports);
  
  /* getter per il valore della Combobox */
  String getSelectValue();
  
  /*Aggiunge un listener al variare della selezione nella Combobox*/
  void onSelectChangeValue(ChangeListener<String> listener);

}
