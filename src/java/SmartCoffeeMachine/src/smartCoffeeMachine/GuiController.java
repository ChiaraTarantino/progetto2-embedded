package smartCoffeeMachine;
import java.util.Arrays;
import java.util.function.Consumer;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

public class GuiController implements Controller {

	@FXML 
	private TextField textField;
	
	@FXML
	private Button button = new Button();
	
	@FXML 
	private ProgressBar pb= new ProgressBar();
	
	@FXML
	private TextArea textArea = new TextArea(); 
	
	@FXML 
	private ComboBox<String> cb = new ComboBox<>();
	
	@FXML 
	private Label labelSugarLevel;

	private static int NMAX = 10;
	private Consumer<String> consumer;
	
	public GuiController() {
	}

	@Override
	public void registerConsumer(Consumer<String> consumer) {
		this.consumer = consumer;
	}
	
	@Override
	public void setProgressBar(float percentage) {
		this.pb.setProgress(percentage);
	}
	
	@Override
	public void println(String msg) {
		this.textArea.appendText(msg + "\n\r");
	}

  @Override
  public void clear() {
    this.textArea.clear();
  }

	@Override
	public void isInMaintenance(boolean maintenance) {
		this.button.setDisable(!maintenance);
	}
	
	@Override
	public void fillSelect(String[] ports) {
        ObservableList<String> portList = FXCollections.observableArrayList();
        Arrays.stream(ports).forEach(portList::add);
        cb.getItems().addAll(portList);
        cb.setValue(portList.get(0)); //setta come elemento la 1° porta trovata
	}
  
	@Override
	public String getSelectValue() {
		return this.cb.getValue();
	}
	
	@Override
	public void onSelectChangeValue(ChangeListener<String> listener) {
		this.cb.valueProperty().addListener(listener);
	}
	
	
	@FXML
	private void handleSubmit(ActionEvent event) {
		if (consumer != null && checkInput()) {
			consumer.accept(this.textField.getText());
		}
		this.textField.clear();
	}
  
	private boolean checkInput() {
		if(textField.getText().length()>0 && Integer.parseInt(textField.getText()) <= NMAX) {
			return true;
		} else {
			Alert alert= new Alert(AlertType.WARNING, "Inserire un valore di capsule inferiore o uguale a 10", ButtonType.OK);
			alert.setHeaderText("ATTENZIONE");
			alert.showAndWait();
			return false;
		}
	}
}

